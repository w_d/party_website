# frozen_string_literal: true

require 'test_helper'

module Admin
  class SessionsControllerTest < ActionDispatch::IntegrationTest
    def test_new
      get login_url
      assert_response :success
    end

    def test_create
      params = {
        email: 'user_to_login_as@email.example',
        password: 'some password'
      }

      create(:user, params)
      post admin_sessions_url, params: params
      assert_redirected_to admin_root_url
    end

    def test_destroy
      user = create(:user, password: 'password')
      login(email: user.email, password: 'password')

      post logout_url
      assert_redirected_to login_url
    end
  end
end
