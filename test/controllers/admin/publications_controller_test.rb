# frozen_string_literal: true

require 'test_helper'

module Admin
  class PublicationsControllerTest < ActionDispatch::IntegrationTest
    def setup
      user = create(:user, password: 'password')
      login(email: user.email, password: 'password')
    end

    def attributes_for_picture
      class_under_test = self.class.to_s[/(\w*)Test$/, 1]
      path = File.join('files', "#{class_under_test.downcase}.jpg")

      picture_attributes = attributes_for(:picture)
      picture_attributes[:file] = fixture_file_upload(path, 'image/jpeg')

      {picture_attributes: picture_attributes}
    end

    class GeneralTest < Admin::PublicationsControllerTest
      def test_index_with_no_publications
        get admin_publications_url
        assert_response :success
      end

      def test_index_with_some_publications
        create(:statement)
        create(:news)
        get admin_publications_url
        assert_response :success
      end

      def test_new
        get new_admin_publication_url
        assert_response :success
      end
    end

    class ArticleTest < PublicationsControllerTest
      def test_create
        params = {
          article: attributes_for(:article).merge(attributes_for_picture),
          type: 'Article'
        }

        assert_difference('Article.count') do
          post admin_publications_url, params: params
        end
        assert_redirected_to admin_publications_url
      end

      def test_edit
        article = create(:article)
        get edit_admin_publication_url(article)
        assert_response :success
      end

      def test_update
        article = create(:article)
        params = {
          article: {
            title: 'Some other title'
          },
          type: 'Article'
        }

        patch admin_publication_url(article), params: params
        assert_redirected_to admin_publications_url
      end

      def test_update_picture
        article = create(:article)
        params = {
          article: attributes_for_picture,
          type: 'Article'
        }

        patch admin_publication_url(article), params: params
        assert_redirected_to admin_publications_url
      end

      def test_destroy
        article = create(:article)
        assert_difference('Article.count', -1) do
          delete admin_publication_url(article)
        end
        assert_redirected_to admin_publications_url
      end
    end

    class NewsTest < PublicationsControllerTest
      def test_create
        params = {
          news: attributes_for(:news).merge(attributes_for_picture),
          type: 'News'
        }

        assert_difference('News.count') do
          post admin_publications_url, params: params
        end
        assert_redirected_to admin_publications_url
      end

      def test_edit
        news = create(:news)
        get edit_admin_publication_url(news)
        assert_response :success
      end

      def test_update
        news = create(:news)
        params = {
          news: {
            title: 'Some other title'
          },
          type: 'News'
        }

        patch admin_publication_url(news), params: params
        assert_redirected_to admin_publications_url
      end

      def test_update_picture
        news = create(:news)
        params = {
          news: attributes_for_picture,
          type: 'News'
        }

        patch admin_publication_url(news), params: params
        assert_redirected_to admin_publications_url
      end

      def test_destroy
        news = create(:news)
        assert_difference('News.count', -1) do
          delete admin_publication_url(news)
        end
        assert_redirected_to admin_publications_url
      end
    end

    class StatementTest < PublicationsControllerTest
      def test_create
        params = {
          statement: attributes_for(:statement).merge(attributes_for_picture),
          type: 'Statement'
        }

        assert_difference('Statement.count') do
          post admin_publications_url, params: params
        end
        assert_redirected_to admin_publications_url
      end

      def test_edit
        statement = create(:statement)
        get edit_admin_publication_url(statement)
        assert_response :success
      end

      def test_update
        statement = create(:statement)
        params = {
          statement: {
            title: 'Some other title'
          },
          type: 'Statement'
        }

        patch admin_publication_url(statement), params: params
        assert_redirected_to admin_publications_url
      end

      def test_update_picture
        statement = create(:statement)
        params = {
          statement: attributes_for_picture,
          type: 'Statement'
        }

        patch admin_publication_url(statement), params: params
        assert_redirected_to admin_publications_url
      end

      def test_destroy
        statement = create(:statement)
        assert_difference('Statement.count', -1) do
          delete admin_publication_url(statement)
        end
        assert_redirected_to admin_publications_url
      end
    end
  end
end
