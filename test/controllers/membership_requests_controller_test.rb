# frozen_string_literal: true

require 'test_helper'

class MembershipRequestsControllerTest < ActionDispatch::IntegrationTest
  include ActiveJob::TestHelper

  def test_new
    get new_membership_request_url
    assert_response :success
  end

  def test_create
    params = {
      membership_request: build(:membership_request).attributes
    }

    assert_difference('MembershipRequest.count') do
      post membership_requests_url, params: params
    end
    assert_redirected_to success_url
  end

  def test_that_mailer_is_called_if_email_is_present
    params = {
      membership_request: build(:membership_request).attributes
    }
    params[:membership_request][:email] = 'some@email.example'

    assert_difference('ActionMailer::Base.deliveries.size') do
      post membership_requests_url, params: params
    end

    mail = ActionMailer::Base.deliveries.last
    assert_equal [params[:membership_request][:email]], mail.to
    assert_equal I18n.t('membership_request_mailer.follow_up.subject'), mail.subject
  end

  def test_that_mailer_is_not_called_without_email_provided
    params = {
      membership_request: build(:membership_request).attributes
    }

    assert_no_difference('ActionMailer::Base.deliveries.size') do
      post membership_requests_url, params: params
    end
  end

  def test_that_mailer_is_not_called_unless_record_saved
    params = {
      membership_request: MembershipRequest.new.attributes
    }

    assert_no_difference('ActionMailer::Base.deliveries.size') do
      post membership_requests_url, params: params
    end
  end

  def test_if_telegram_job_is_queued
    params = {
      membership_request: build(:membership_request).attributes
    }

    assert_enqueued_with(job: NotifyWithTelegramJob) do
      post membership_requests_url, params: params
    end
  end

  def test_if_trello_job_is_queued
    params = {
      membership_request: build(:membership_request).attributes
    }

    assert_enqueued_with(job: CreateCardOnTrelloJob) do
      post membership_requests_url, params: params
    end
  end

  def test_that_no_jobs_are_queued_unless_record_is_saved
    params = {
      membership_request: MembershipRequest.new.attributes
    }

    assert_no_enqueued_jobs do
      post membership_requests_url, params: params
    end
  end
end
