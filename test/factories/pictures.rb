# frozen_string_literal: true

FactoryBot.define do
  factory :picture do
    association :picturable, factory: :illustration

    file do
      Rack::Test::UploadedFile.new(
        Rails.root.join('test', 'fixtures', 'files', 'ancap-smiley.jpg'),
        'image/jpg'
      )
    end
  end
end
