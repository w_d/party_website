# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:name) { |n| "user#{n}" }
    sequence(:email) { |n| "user#{n}@email.example" }
    password 'password'
    password_confirmation { password }
    invite
  end
end
