# frozen_string_literal: true

FactoryBot.define do
  factory :article do
    association :author, factory: :user
    association :picture

    annotation 'Основные тезисы, история развития и политическое влияние'
    body 'В романах «Источник» и «Атлант расправил плечи» Рэнд вывела…'
    published_at { Date.yesterday }
    title 'Объективизм по Айн Рэнд'
    type 'Article'
  end
end
