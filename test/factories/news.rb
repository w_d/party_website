# frozen_string_literal: true

FactoryBot.define do
  factory :news do
    association :author, factory: :user
    association :picture

    annotation 'Информация обновляется и дополняется'
    body 'В субботу по всей стране прошли протестные акции оппозиции…'
    published_at { Date.yesterday }
    title 'Митинги 5-го мая'
    type 'News'
  end
end
