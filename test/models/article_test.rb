# frozen_string_literal: true

require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  def test_that_factory_defines_valid_article
    article = build(:article)
    assert article.valid?
  end

  def test_requirement_for_author_presence
    article = build(:article, author: nil)
    refute article.valid?
  end

  def test_article_without_picture
    article = build(:article, picture: nil)
    refute article.valid?
  end

  def test_if_article_is_valid_without_title
    article = build(:article, title: '')
    refute article.valid?
  end

  def test_that_body_is_required
    article = build(:article, body: '')
    refute article.valid?
  end

  def test_published_at_presence_validation
    article = build(:article, published_at: nil)
    refute article.valid?
  end

  def test_that_annotation_is_required
    article = build(:article, annotation: '')
    refute article.valid?
  end

  def test_that_too_long_annotation_is_rejected
    article = build(:article, annotation: 'A' * 141)
    refute article.valid?
  end
end
