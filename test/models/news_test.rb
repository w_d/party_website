# frozen_string_literal: true

require 'test_helper'

class NewsTest < ActiveSupport::TestCase
  def test_that_factory_defines_valid_news
    news = build(:news)
    assert news.valid?
  end

  def test_requirement_for_author_presence
    news = build(:news, author: nil)
    refute news.valid?
  end

  def test_news_without_picture
    news = build(:news, picture: nil)
    refute news.valid?
  end

  def test_if_news_is_valid_without_title
    news = build(:news, title: '')
    refute news.valid?
  end

  def test_that_body_is_required
    news = build(:news, body: '')
    refute news.valid?
  end

  def test_published_at_presence_validation
    news = build(:news, published_at: nil)
    refute news.valid?
  end

  def test_that_annotation_is_required
    news = build(:news, annotation: '')
    refute news.valid?
  end

  def test_that_too_long_annotation_is_rejected
    news = build(:news, annotation: 'A' * 141)
    refute news.valid?
  end
end
