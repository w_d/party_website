# frozen_string_literal: true

require 'test_helper'

class MembershipRequestMailerTest < ActionMailer::TestCase
  def test_follow_up
    membership_request = build(
      :membership_request,
      email: 'provided@email.example',
      full_name: 'Full Name'
    )

    mail = MembershipRequestMailer.follow_up(membership_request)

    assert_emails 1 do
      mail.deliver_now
    end

    assert_equal ['notifications@new.libertarian-party.ru'], mail.from
    assert_equal [membership_request.email], mail.to
    assert_equal I18n.t('membership_request_mailer.follow_up.subject'), mail.subject

    assert_equal read_fixture('follow_up.html').join, mail.html_part.body.to_s
    assert_equal read_fixture('follow_up.txt').join, mail.text_part.body.to_s
  end
end
