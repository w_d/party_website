# frozen_string_literal: true

class CreateFederalSubjects < ActiveRecord::Migration[5.1]
  def change
    create_table :federal_subjects, id: :uuid do |t|
      t.string :name

      t.timestamps
    end
  end
end
