# frozen_string_literal: true

class RemovePaperclipFieldsFromPublication < ActiveRecord::Migration[5.2]
  def change
    remove_column :publications, :preview_image_fingerprint, :string
    remove_column :publications, :preview_image_file_name, :string
    remove_column :publications, :preview_image_content_type, :string
    remove_column :publications, :preview_image_file_size, :integer
    remove_column :publications, :preview_image_updated_at, :datetime
  end
end
