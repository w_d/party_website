# frozen_string_literal: true

class AddPreviewTextToNews < ActiveRecord::Migration[5.1]
  def change
    add_column :news, :preview_text, :string
  end
end
