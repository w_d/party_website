# frozen_string_literal: true

class RenamePreviewTextToAnnotation < ActiveRecord::Migration[5.1]
  def change
    rename_column :publications, :preview_text, :annotation
  end
end
