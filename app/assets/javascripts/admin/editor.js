document.addEventListener("turbolinks:load", function() {
  var form = document.getElementById("publication-form");

  if (form) {
    var wrapper = document.getElementById("editor-wrapper");
    var textarea = document.getElementById("publication_body");

    var editor = new Yamete(wrapper, textarea.value);
    form.addEventListener("submit", function() {
      textarea.value = editor.content;
    });
  }
});
