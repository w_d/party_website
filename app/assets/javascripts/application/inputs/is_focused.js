/* globals Util */

document.addEventListener("turbolinks:load", function () {
  var inputs = Util.selectAll(".form-input");

  if (inputs.length !== 0) {
    document.addEventListener("focusin", function (event) {
      if (inputs.includes(event.target)) {
        Util.parentWithClass("form-field", event.target)
          .classList.add("is-focused");
      }
    });

    document.addEventListener("focusout", function (event) {
      if (inputs.includes(event.target)) {
        Util.parentWithClass("form-field", event.target)
          .classList.remove("is-focused");
      }
    });
  }
});
