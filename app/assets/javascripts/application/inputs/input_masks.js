/* globals Util */

document.addEventListener("turbolinks:load", function() {
  var applyDateMask = function (el) {
    var date = el.value;
    var caret = el.selectionStart;
    var unallowedChars = /[^\.\d]/g;

    var firstDot = 2, secondDot = 5;
    var i;

    /* Don't progress the caret if unallowedChars are typed. */
    if (date.substr(0, caret).match(unallowedChars)) {
      --caret;
    }

    /* Delete unallowedChars. */
    date = date.replace(unallowedChars, "");

    for (i = 0; i < date.length; ++i) {
      if (date.charAt(i) === ".") {
        /* Remove dots from wrong positions. */
        if (i !== firstDot && i !== secondDot) {
          date = date.substr(0, i) + date.substr(i + 1);
          --caret;
        }
      } else {
        /* Ensure dots exist in appropriate positions. */
        if (i === firstDot || i === secondDot) {
          date = date.substr(0, i) + "." + date.substr(i);
          ++caret;
        }
      }
    }

    el.value = date;

    /* Position the caret. */
    el.selectionEnd = caret;
  };

  var applyPhoneMask = function (el) {
    var phone = el.value;
    var caret = el.selectionStart;
    var unallowedChars = /(?!^)\+|[^\+\d]/g;

    /* Add a plus sign to the start of a number. */
    if (phone.length > 0 && !phone.startsWith("+")) {
      phone = "+" + phone;
      ++caret;
    }

    /* Don't progress the caret if unallowedChars are typed. */
    if (phone.substr(0, caret).match(unallowedChars)) {
      --caret;
    }

    /* Delete unallowedChars. */
    el.value = phone.replace(unallowedChars, "");

    /* Position the caret. */
    el.selectionEnd = caret;
  };

  var inputs = Util.selectAll("[data-format]");

  if (inputs.length !== 0) {
    document.addEventListener("input", function (event) {
      if (inputs.includes(event.target)) {
        switch(event.target.dataset.format) {
          case "date":
            applyDateMask(event.target);
            break;
          case "phone":
            applyPhoneMask(event.target);
            break;
        }
      }
    });
  }
});
