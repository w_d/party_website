/* globals Util */

document.addEventListener("turbolinks:load", function () {
  var multilines = Util.selectAll(".form-multiline");
  var field, mirror;

  if (multilines.length !== 0) {
    multilines.forEach(function (multiline) {
      field = multiline.parentElement;
      mirror = document.createElement("div");

      field.insertBefore(mirror, multiline);

      mirror.appendChild(document.createElement("span"));
      mirror.appendChild(document.createElement("br"));

      mirror.classList.add("form-mirror");
      mirror.querySelector("span").textContent = multiline.value;
    });

    document.addEventListener("input", function (event) {
      if (multilines.includes(event.target)) {
        field = event.target.parentElement;
        mirror = field.querySelector(".form-mirror");

        mirror.querySelector("span").textContent = event.target.value;
      }
    });
  }
});
