# frozen_string_literal: true

class Illustration < ApplicationRecord
  has_one :picture, as: :picturable

  validates :caption, length: {maximum: 140}
end
