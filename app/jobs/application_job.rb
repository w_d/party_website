# frozen_string_literal: true

class ApplicationJob < ActiveJob::Base
  def present(object, klass = nil)
    klass ||= "#{object.class}Presenter".constantize
    klass.new(object, ApplicationController.renderer)
  end
end
