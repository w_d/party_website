# frozen_string_literal: true

class CreateCardOnTrelloJob < ApplicationJob
  queue_as :trello

  def perform(membership_request)
    membership_request = present(membership_request)

    params = {
      list_id: find_list_id(membership_request.federal_subject),
      name: membership_request.summary,
      desc: membership_request.render,
      due: Date.today + 1.week
    }

    Trello::Card.create(params)
  end

  private

  def find_list_id(federal_subject)
    federal_subject.regional_branch&.trello_list || ENV['TRELLO_FEDERAL_LIST']
  end
end
