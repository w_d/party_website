# frozen_string_literal: true

module Admin
  class PublicationsController < BaseController
    before_action :set_publication, only: %i[edit update destroy]

    def index
      @publications = Publication
                      .reorder(created_at: :desc)
                      .page(params[:page])
                      .per(9)
    end

    def new
      return unless publication_type

      @publication = publication_type.constantize.new
      @publication.build_picture
    end

    def create
      @publication = publication_type.constantize.new(publication_params)
      @publication.author = current_user

      if @publication.save
        redirect_to admin_publications_url, notice: t('.success')
      else
        render :new
      end
    end

    def edit
    end

    def update
      if @publication.update(publication_params)
        redirect_to admin_publications_url, notice: t('.success')
      else
        render :edit
      end
    end

    def destroy
      @publication.destroy
      redirect_to admin_publications_url, notice: t('.success')
    end

    private

    def set_publication
      @publication = Publication.find(params[:id])
    end

    def publication_type
      @publication_type ||= params[:type] if params[:type].in?(Publication.types)
    end

    def publication_params
      params
        .require(publication_type.downcase)
        .permit(
          :annotation,
          :body,
          :published_at,
          :title,
          picture_attributes: [:file]
        )
    end
  end
end
