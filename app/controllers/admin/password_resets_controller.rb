# frozen_string_literal: true

module Admin
  class PasswordResetsController < BaseController
    skip_before_action :require_login

    def new
    end

    def create
      user = User.find_by_email(params[:email].downcase)
      user && UserMailer.reset_password(user).deliver_later

      redirect_to login_url, notice: t('.check_your_email')
    end
  end
end
