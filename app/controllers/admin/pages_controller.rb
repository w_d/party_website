# frozen_string_literal: true

module Admin
  class PagesController < BaseController
    def home
      redirect_to admin_publications_url
    end
  end
end
