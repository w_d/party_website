# frozen_string_literal: true

module Admin
  class BaseController < ActionController::Base
    protect_from_forgery with: :exception

    before_action :require_login

    def require_admin
      not_admin unless current_user.admin?
    end

    private

    layout 'admin'

    def not_admin
      redirect_back fallback_location: admin_root_url, alert: t('forbidden')
    end

    # When changing this method's name, don't forget to change Sorcery config.
    def not_logged_in
      redirect_to login_url, alert: t('please_login')
    end
  end
end
