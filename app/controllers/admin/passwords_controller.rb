# frozen_string_literal: true

module Admin
  class PasswordsController < BaseController
    skip_before_action :require_login

    before_action :set_token
    before_action :set_user
    before_action :reject_invalid_token

    def edit
    end

    def update
      @user.password_confirmation = params[:password_confirmation]

      if @user.change_password!(params[:password])
        redirect_to login_url, notice: t('.success')
      else
        render :edit
      end
    end

    private

    def set_token
      @token = params[:token]
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    def reject_invalid_token
      return if @token && @user == User.load_from_reset_password_token(@token)

      render :problem_with_token, status: :bad_request
    end
  end
end
