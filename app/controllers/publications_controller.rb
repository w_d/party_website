# frozen_string_literal: true

class PublicationsController < ApplicationController
  def index
    @publications = Publication
                    .reorder(published_at: :desc)
                    .page(params[:page])
                    .per(9)
  end

  def show
    @publication = Publication.find(params[:id])
  end
end
