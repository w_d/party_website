# frozen_string_literal: true

I18n.backend.class.include(I18n::Backend::Pluralization)
I18n.load_path = Dir.glob(
  Rails.root.join('config', 'locales', '**', '*.{rb,yml}')
)
I18n.default_locale = :ru
