# frozen_string_literal: true

set :chruby_ruby, 'ruby-2.5.3'

server '188.40.248.3', user: fetch(:user), roles: %w[app db web]
