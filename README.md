# Сайт Либертарианской партии

По вопросам, связанным с сайтом партии, пишите в телеграм
[@akater](https://t.me/akater) или [@oleynikov](https://t.me/oleynikov).

## Сообщить о баге
[Начните обсуждение][new_issue]. Опишите шаги, которые привели к проблеме.
Прикрепите скриншоты. Чем больше вы предоставите информации, тем быстрее мы
всё починим. Потребуется зарегистрироваться либо войти через Google, Twitter
или GitHub.

## Поучаствовать в разработке
Помощь приветствуется, присылайте патчи :-)

Установите:
- Ruby 2.5,
- Bundler,
- Yarn,
- ImageMagick,
- PostgreSQL.

Скопируйте репозиторий и исполните setup-скрипт.
```
$ git clone git@gitlab.com:libertarian-party/party_website.git
$ cd party_website/
$ bin/rake project:setup
```

Можно приступать к работе. Например, можете взяться за любую из
[задач с пометкой feature][features].

[new_issue]: https://gitlab.com/libertarian-party/party_website/issues/new
[features]: https://gitlab.com/libertarian-party/party_website/issues?scope=all&utf8=✓&state=opened&assignee_id=0&label_name[]=feature
