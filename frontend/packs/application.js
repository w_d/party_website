import ReactRailsUJS from 'react_ujs'

import '../blocks/Body'

ReactRailsUJS.useContext(require.context('../blocks', true, /^\.\/\w+$/))
