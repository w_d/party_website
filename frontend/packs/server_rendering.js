// By default, this pack is loaded for server-side rendering.
// It must expose react_ujs as `ReactRailsUJS` and prepare a require context.
import ReactRailsUJS from 'react_ujs'

import '../blocks/Body'

ReactRailsUJS.useContext(require.context('../blocks', true, /^\.\/\w+$/))
