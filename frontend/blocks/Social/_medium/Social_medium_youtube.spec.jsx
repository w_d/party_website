import React from 'react'
import { shallow } from 'enzyme'

import { Social as BaseSocial } from '../Social'
import { SocialMediumYoutube } from './Social_medium_youtube'

const Social = SocialMediumYoutube(BaseSocial)

describe('Social_medium_youtube', () => {
  const socialMediumYoutube = shallow(<Social medium='youtube' />)

  it('has classes Social and Social_medium_youtube', () => {
    expect(socialMediumYoutube.prop('className'))
      .toBe('Social Social_medium_youtube')
  })

  it('contains an icon', () => {
    expect(socialMediumYoutube.prop('icon')).toBeTruthy()
  })
})
