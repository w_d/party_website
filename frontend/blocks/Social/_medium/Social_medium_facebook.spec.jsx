import React from 'react'
import { shallow } from 'enzyme'

import { Social as BaseSocial } from '../Social'
import { SocialMediumFacebook } from './Social_medium_facebook'

const Social = SocialMediumFacebook(BaseSocial)

describe('Social_medium_facebook', () => {
  const socialMediumFacebook = shallow(<Social medium='facebook' />)

  it('by default has classes Social and Social_medium_facebook', () => {
    expect(socialMediumFacebook.prop('className'))
      .toBe('Social Social_medium_facebook')
  })

  it('contains an icon', () => {
    expect(socialMediumFacebook.prop('icon')).toBeTruthy()
  })
})
