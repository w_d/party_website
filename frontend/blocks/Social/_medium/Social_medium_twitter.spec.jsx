import React from 'react'
import { shallow } from 'enzyme'

import { Social as BaseSocial } from '../Social'
import { SocialMediumTwitter } from './Social_medium_twitter'

const Social = SocialMediumTwitter(BaseSocial)

describe('Social_medium_twitter', () => {
  const socialMediumTwitter = shallow(<Social medium='twitter' />)

  it('has classes Social and Social_medium_twitter', () => {
    expect(socialMediumTwitter.prop('className'))
      .toBe('Social Social_medium_twitter')
  })

  it('contains an icon', () => {
    expect(socialMediumTwitter.prop('icon')).toBeTruthy()
  })
})
