import React from 'react'
import { shallow } from 'enzyme'

import { LogoText } from './Logo-Text'

describe('Logo-Text', () => {
  const logoText = shallow(<LogoText />)

  it('renders nothing', () => {
    expect(logoText.type()).toBe(null)
  })
})

describe('Logo-Text with children', () => {
  const logoText = shallow(<LogoText>hello there</LogoText>)

  it('is <span>', () => {
    expect(logoText.is('span')).toBe(true)
  })

  it('has class Logo-Text', () => {
    expect(logoText.prop('className')).toBe('Logo-Text')
  })

  it('contains provided children', () => {
    expect(logoText.text()).toBe('hello there')
  })
})
