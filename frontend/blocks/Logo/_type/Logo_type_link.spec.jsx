import React from 'react'
import { shallow } from 'enzyme'

import { Logo as BaseLogo } from '../Logo'
import { LogoIcon } from '../-Icon/Logo-Icon'
import { LogoText } from '../-Text/Logo-Text'
import { LogoTypeLink } from './Logo_type_link'

const Logo = LogoTypeLink(BaseLogo)

describe('Logo_type_link', () => {
  const logoTypeLink = shallow(<Logo type='link' />)

  it('is <a>', () => {
    expect(logoTypeLink.is('a')).toBe(true)
  })

  it('has classes Logo and Logo_type_link by default', () => {
    expect(logoTypeLink.prop('className')).toBe('Logo Logo_type_link')
  })

  it('has an undefined href by default', () => {
    expect(logoTypeLink.prop('href')).toBeUndefined()
  })

  it('contains Logo-Icon', () => {
    expect(logoTypeLink.contains(<LogoIcon />)).toBe(true)
  })

  it('contains Logo-Text', () => {
    expect(logoTypeLink.contains(<LogoText />)).toBe(true)
  })
})

describe('Logo_type_link with extra classes', () => {
  const logoTypeLink = shallow(<Logo type='link' className='Abc Xyz' />)

  it('appends that classes to the default class', () => {
    expect(logoTypeLink.prop('className'))
      .toBe('Logo Logo_type_link Abc Xyz')
  })
})

describe('Logo_type_link with iconTheme', () => {
  const logoTypeLink = shallow(<Logo type='link' iconTheme='some-theme' />)

  it('passes that as theme to Logo-Icon', () => {
    expect(logoTypeLink.contains(<LogoIcon theme='some-theme' />)).toBe(true)
  })
})

describe('Logo_type_link with children', () => {
  const logoTypeLink = shallow(<Logo type='link'>with children</Logo>)

  it('passes that children to Logo-Text', () => {
    const logoText = <LogoText>with children</LogoText>
    expect(logoTypeLink.contains(logoText)).toBe(true)
  })
})
