import { compose } from '@bem-react/core'

import { Logo as BaseLogo } from './Logo'
import { LogoThemeDefault } from './_theme/Logo_theme_default'
import { LogoThemeNegative } from './_theme/Logo_theme_negative'
import { LogoTypeLink } from './_type/Logo_type_link'

const Logo = compose(
  LogoThemeDefault,
  LogoThemeNegative,
  LogoTypeLink
)(BaseLogo)

export default Logo
