import React from 'react'
import { shallow } from 'enzyme'

import { BaseLogoIcon } from '../Logo-Icon'
import { LogoIconThemeNegative } from './Logo-Icon_theme_negative'

const LogoIcon = LogoIconThemeNegative(BaseLogoIcon)

describe('Logo-Icon_theme_negative', () => {
  const logoIconThemeNegative = shallow(<LogoIcon theme='negative' />)

  it('has classes Logo-Icon and Logo-Icon_theme_negative', () => {
    expect(logoIconThemeNegative.prop('className'))
      .toBe('Logo-Icon Logo-Icon_theme_negative')
  })
})
