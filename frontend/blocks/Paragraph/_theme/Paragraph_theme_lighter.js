import { withBemMod } from '@bem-react/core'

import { cnParagraph } from '../Paragraph'
import './Paragraph_theme_lighter.css'

export const ParagraphThemeLighter = withBemMod(
  cnParagraph(),
  { theme: 'lighter' }
)
