import React from 'react'
import { shallow } from 'enzyme'

import { ContactsItem } from '../-Item/Contacts-Item'
import { ContactsLabel } from './Contacts-Label'

describe('Contacts-Label', () => {
  const contactsLabel = shallow(<ContactsLabel />)

  it('is null by default', () => {
    expect(contactsLabel.type()).toBe(null)
  })
})

describe('Contacts-Label with false children', () => {
  const contactsLabel = shallow(<ContactsLabel>{false}</ContactsLabel>)

  it('is still null', () => {
    expect(contactsLabel.type()).toBe(null)
  })
})

describe('Contacts-Label with children', () => {
  const contactsLabel = shallow(<ContactsLabel><p>text</p></ContactsLabel>)

  it('is Contacts-Item', () => {
    expect(contactsLabel.is(ContactsItem)).toBe(true)
  })

  it('has class Contacts-Label', () => {
    expect(contactsLabel.prop('className')).toBe('Contacts-Label')
  })

  it('contains children', () => {
    expect(contactsLabel.children().length).toBe(1)
    expect(contactsLabel.contains(<p>text</p>)).toBe(true)
  })
})
