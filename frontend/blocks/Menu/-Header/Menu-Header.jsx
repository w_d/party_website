import React from 'react'

import Close from '../../Close'
import Logo from '../../Logo'
import { cnMenu } from '../Menu'
import './Menu-Header.css'

export const MenuHeader = (props) => (
  <div className={cnMenu('Header')}>
    <Logo theme='default' type='link' uri='/'>ЛПР</Logo>
    <Close onClick={props.onCloseClick} />
  </div>
)
