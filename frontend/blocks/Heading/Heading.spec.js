import React from 'react'
import { shallow } from 'enzyme'

import { Heading } from './Heading'

describe('Heading', () => {
  const heading = shallow(<Heading />)

  it('is <span> by default', () => {
    expect(heading.is('span')).toBe(true)
  })

  it('has undefined tag by default', () => {
    expect(heading.prop('tag')).toBeUndefined()
  })

  it('has class Heading by default', () => {
    expect(heading.prop('className')).toBe('Heading')
  })

  it('is empty by default', () => {
    expect(heading.children().length).toBe(0)
  })
})

describe('Heading with tag', () => {
  const heading = shallow(<Heading tag='h1' />)

  it('now uses that tag', () => {
    expect(heading.is('h1')).toBe(true)
  })
})

describe('Heading with more classes', () => {
  const heading = shallow(<Heading className='Hey There' />)

  it('appends these classes to the dafault one', () => {
    expect(heading.prop('className')).toBe('Heading Hey There')
  })
})

describe('Heading with some arbitrary props', () => {
  const heading = shallow(<Heading id='the-heading' dataHello='hi' />)

  it('includes them too', () => {
    expect(heading.prop('id')).toBe('the-heading')
    expect(heading.prop('dataHello')).toBe('hi')
  })
})

describe('Heading with children', () => {
  const heading = shallow(<Heading>your text could be here</Heading>)

  it('incorporates them', () => {
    expect(heading.children().length).toBe(1)
    expect(heading.text()).toBe('your text could be here')
  })
})
