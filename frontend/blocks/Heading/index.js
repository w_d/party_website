import { compose } from '@bem-react/core'

import { Heading as BaseHeading } from './Heading'
import { HeadingSizeL } from './_size/Heading_size_l'
import { HeadingSizeM } from './_size/Heading_size_m'
import { HeadingSizeS } from './_size/Heading_size_s'
import { HeadingSizeXL } from './_size/Heading_size_xl'

const Heading = compose(
  HeadingSizeL,
  HeadingSizeM,
  HeadingSizeS,
  HeadingSizeXL
)(BaseHeading)

export default Heading
