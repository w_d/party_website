import React from 'react'
import { shallow } from 'enzyme'

import { Heading as BaseHeading } from '../Heading'
import { HeadingSizeXL } from './Heading_size_xl'

const Heading = HeadingSizeXL(BaseHeading)

describe('Heading_size_xl', () => {
  const headingSizeXL = shallow(<Heading size='xl' />)

  it('has classes Heading and Heading_size_xl', () => {
    expect(headingSizeXL.prop('className')).toBe('Heading Heading_size_xl')
  })
})
