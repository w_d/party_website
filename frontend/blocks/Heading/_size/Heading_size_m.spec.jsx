import React from 'react'
import { shallow } from 'enzyme'

import { Heading as BaseHeading } from '../Heading'
import { HeadingSizeM } from './Heading_size_m'

const Heading = HeadingSizeM(BaseHeading)

describe('Heading_size_m', () => {
  const headingSizeM = shallow(<Heading size='m' />)

  it('has classes Heading and Heading_size_m', () => {
    expect(headingSizeM.prop('className')).toBe('Heading Heading_size_m')
  })
})
