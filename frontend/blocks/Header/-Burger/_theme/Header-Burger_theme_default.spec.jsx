import React from 'react'
import { shallow } from 'enzyme'

import { BaseHeaderBurger } from '../Header-Burger'
import { HeaderBurgerThemeDefault } from './Header-Burger_theme_default'
import { BurgerIcon } from './icons/Burger'

const HeaderBurger = HeaderBurgerThemeDefault(BaseHeaderBurger)

describe('Header-Burger_theme_default', () => {
  const headerBurgerThemeDefault = shallow(<HeaderBurger theme='default' />)

  it('has classes Header-Burger and Header-Burger_theme_default', () => {
    expect(headerBurgerThemeDefault.prop('className'))
      .toBe('Header-Burger Header-Burger_theme_default')
  })

  it('contains burger icon', () => {
    expect(headerBurgerThemeDefault.prop('icon')).toMatchObject(<BurgerIcon />)
  })
})
