import React from 'react'
import { shallow } from 'enzyme'

import { HeaderSections } from './Header-Sections'
import { HeaderLink } from '../-Link/Header-Link'

describe('Header-Sections', () => {
  const headerSections = shallow(<HeaderSections />)

  it('is <div>', () => {
    expect(headerSections.is('div')).toBe(true)
  })

  it('has class Header-Sections', () => {
    expect(headerSections.prop('className')).toBe('Header-Sections')
  })

  it('includes Header-Link to /publications with text "Публикации"', () => {
    const link = (
      <HeaderLink active={false} uri='/publications'>
        Публикации
      </HeaderLink>
    )
    expect(headerSections.contains(link)).toBe(true)
  })

  it('contains Header-Link to /party with text "Партия"', () => {
    const link = (
      <HeaderLink active={false} uri='/party'>
        Партия
      </HeaderLink>
    )
    expect(headerSections.contains(link)).toBe(true)
  })

  it('has Header-Link to /libertarianism with text "Либертарианство"', () => {
    const link = (
      <HeaderLink active={false} uri='/libertarianism'>
        Либертарианство
      </HeaderLink>
    )
    expect(headerSections.contains(link)).toBe(true)
  })
})

describe('Header-Sections with activeLink set to "publications"', () => {
  const headerSections = shallow(<HeaderSections activeLink='publications' />)

  it('contains active Header-Link to /publications', () => {
    const link = (
      <HeaderLink active uri='/publications'>
        Публикации
      </HeaderLink>
    )
    expect(headerSections.contains(link)).toBe(true)
  })
})

describe('Header-Sections with "party" as activeLink', () => {
  const headerSections = shallow(<HeaderSections activeLink='party' />)

  it('includes active Header-Link to /party', () => {
    const link = (
      <HeaderLink active uri='/party'>
        Партия
      </HeaderLink>
    )
    expect(headerSections.contains(link)).toBe(true)
  })
})

describe('Header-Sections with "libertarianism" as activeLink', () => {
  const headerSections = shallow(<HeaderSections activeLink='libertarianism' />)

  it('has Header-Link to /libertarianism that is active', () => {
    const link = (
      <HeaderLink active uri='/libertarianism'>
        Либертарианство
      </HeaderLink>
    )
    expect(headerSections.contains(link)).toBe(true)
  })
})
