import React from 'react'
import { shallow } from 'enzyme'

import { HeaderActions } from './Header-Actions'
import { HeaderLink } from '../-Link/Header-Link'

describe('Header-Actions', () => {
  const headerActions = shallow(<HeaderActions />)

  it('is <div>', () => {
    expect(headerActions.is('div')).toBe(true)
  })

  it('has class Header-Actions', () => {
    expect(headerActions.prop('className')).toBe('Header-Actions')
  })

  it('has Header-Link to /join with text "Вступить"', () => {
    const link = (
      <HeaderLink active={false} uri='/join'>
        Вступить
      </HeaderLink>
    )
    expect(headerActions.contains(link)).toBe(true)
  })

  it('contains Header-Link to /donate with text "Поддержать"', () => {
    const link = (
      <HeaderLink active={false} uri='/donate'>
        Поддержать
      </HeaderLink>
    )
    expect(headerActions.contains(link)).toBe(true)
  })
})

describe('Header-Actions with "join" as activeLink', () => {
  const headerActions = shallow(<HeaderActions activeLink='join' />)

  it('includes active Header-Link to /join', () => {
    const link = (
      <HeaderLink active uri='/join'>
        Вступить
      </HeaderLink>
    )
    expect(headerActions.contains(link)).toBe(true)
  })
})

describe('Header-Actions with activeLink set to "donate"', () => {
  const headerActions = shallow(<HeaderActions activeLink='donate' />)

  it('contains Header-Link to /donate that is active', () => {
    const link = (
      <HeaderLink active uri='/donate'>
        Поддержать
      </HeaderLink>
    )
    expect(headerActions.contains(link)).toBe(true)
  })
})
