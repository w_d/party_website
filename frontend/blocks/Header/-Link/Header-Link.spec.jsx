import React from 'react'
import { shallow } from 'enzyme'

import { BaseHeaderLink as HeaderLink } from './Header-Link'

describe('Header-Link', () => {
  const headerLink = shallow(<HeaderLink />)

  it('is <a>', () => {
    expect(headerLink.is('a')).toBe(true)
  })

  it('has class Header-Link by default', () => {
    expect(headerLink.prop('className')).toBe('Header-Link')
  })

  it('has no href by default', () => {
    expect(headerLink.prop('href')).toBeUndefined()
  })

  it('contains no children by default', () => {
    expect(headerLink.children().length).toBe(0)
  })
})

describe('Header-Link with some class', () => {
  const headerLink = shallow(<HeaderLink className='class' />)

  it('has that class in addition to the default one', () => {
    expect(headerLink.prop('className')).toBe('Header-Link class')
  })
})

describe('Header-Link with children', () => {
  const headerLink = shallow(
    <HeaderLink>just some text in place of children</HeaderLink>
  )

  it('incorporates them', () => {
    expect(headerLink.children().length).toBe(1)
    expect(headerLink.text()).toBe('just some text in place of children')
  })
})

describe('Header-Link with some URI', () => {
  const headerLink = shallow(<HeaderLink uri='/path' />)

  it('has that as href', () => {
    expect(headerLink.prop('href')).toBe('/path')
  })
})
