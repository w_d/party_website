import React from 'react'
import { withBemMod } from '@bem-react/core'

import Contact from '../../../Contact'
import Contacts from '../../../Contacts'
import Heading from '../../../Heading'
import Paragraph from '../../../Paragraph'
import Symbols from '../../../Symbols'

import { PageContent } from '../../-Content/Page-Content'
import { PageFooter } from '../../-Footer/Page-Footer'
import { PageHeader } from '../../-Header/Page-Header'
import { cnPage } from '../../Page'

import bazhenov from './photos/bazhenov.jpg'
import bazhenov2x from './photos/bazhenov@2x.jpg'
import blade from './photos/blade.jpg'
import blade2x from './photos/blade@2x.jpg'
import boiko from './photos/boiko.jpg'
import boiko2x from './photos/boiko@2x.jpg'
import svetov from './photos/svetov.jpg'
import svetov2x from './photos/svetov@2x.jpg'
import './Page_name_press.css'

const newBody = (Base, props) => (
  <Base {...props} >
    <PageHeader theme='default' activeLink='party' />
    <PageContent tag='main'>
      <Heading className={cnPage('Head')} size='l' tag='h1'>
        Для прессы
      </Heading>

      <Heading className={cnPage('Sub')} size='s' tag='h2'>
        Пресс-секретарь и спикеры
      </Heading>
      <Contacts className={cnPage('Contacts')}>
        <Contact
          email='press@libertarian-party.ru'
          name='София Блейд'
          photo={blade}
          photo2x={blade2x}
          position='Пресс-секретарь'
          telegram='cannibalica' />
        <Contact
          email='s.boiko@libertarian-party.ru'
          name='Сергей Бойко'
          phone='+7 916 127-43-41'
          photo={boiko}
          photo2x={boiko2x}
          position='Председатель'
          telegram='dsboiko' />
        <Contact
          email='svetov@libertarian-party.ru'
          name='Михаил Светов'
          photo={svetov}
          photo2x={svetov2x}
          position='Член Федерального комитета партии, политолог,
            автор ютьюб&#8209;канала SVTV' />
        <Contact
          name='Григорий Баженов'
          photo={bazhenov}
          photo2x={bazhenov2x}
          position='Кандидат экономических наук, научный сотрудник
            экономического факультета МГУ, член партии'
          telegram='bajenof' />
      </Contacts>

      <div className={cnPage('Dyad')}>
        <div className={cnPage('AboutParty')}>
          <Heading className={cnPage('Sub')} size='s' tag='h2'>
            Коротко о партии
          </Heading>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Либертарианская партия России (ЛПР) возникла в&nbsp;2008 году
            и&nbsp;на&nbsp;начало 2019 года объединяет около 800 членов
            и&nbsp;240 сторонников из&nbsp;72 регионов России. Мы&nbsp;появились
            снизу вверх: никогда не&nbsp;было лидера, вокруг которого
            организовывались&nbsp;бы люди, партию создала группа студентов.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Либертарианство как политическая философия основывается
            на&nbsp;принципах неагрессии и&nbsp;самопринадлежности. Политическая
            и&nbsp;экономическая программа партии определяются исходя
            из&nbsp;них.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Члены партии были избраны в&nbsp;муниципальные депутаты (2012,
            2015, 2017) в&nbsp;Москве и&nbsp;Подмосковье. Мы&nbsp;регулярно
            участвуем в&nbsp;муниципальных выборах, а&nbsp;также участвовали
            в&nbsp;выборах в&nbsp;Московскую Городскую Думу,
            в&nbsp;Государственную Думу, в&nbsp;региональных выборах мэра
            и&nbsp;в&nbsp;Законодательное Собрание.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Регулярно проводим лекции, дебаты, участвуем в&nbsp;Форумах Свободных
            Людей, Чтениях Адама Смита, Мемориальных Конференциях Айн Рэнд.
            Организовали митинги против Роскомнадзора 30&nbsp;апреля 2018
            и&nbsp;против налогового грабежа 29&nbsp;июля 2018 в&nbsp;Москве.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Мы&nbsp;не&nbsp;сотрудничаем с&nbsp;системной оппозицией.
            Государство отказывается выдавать нашей партии регистрацию,
            мы&nbsp;пытались четыре раза, последний в&nbsp;2015&nbsp;году.
            Наша цель&nbsp;— изменение существующей модели государственного
            устройства России мирными средствами в&nbsp;соответствии
            с&nbsp;либертарианскими ценностями.
          </Paragraph>
        </div>

        <div className={cnPage('Other')}>
          <Heading className={cnPage('Sub')} size='s' tag='h2'>
            Символика
          </Heading>
          <Symbols />
        </div>
      </div>
    </PageContent>
    <PageFooter isPressButtonDisabled />
  </Base>
)

export const PageNamePress = withBemMod(
  cnPage(),
  { name: 'press' },
  newBody
)
